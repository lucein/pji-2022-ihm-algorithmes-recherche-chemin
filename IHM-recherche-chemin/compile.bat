mkdir bin

JDK-8\bin\javac.exe -d bin src/view/*.java src/controller/*.java src/model/*.java src/model/labyrinth/*.java src/model/algorithm/*.java src/resources/*.java

cd bin

..\JDK-8\bin\jar.exe -cvfe ../IHM-recherche-chemin.jar view.Main -C view controller .

cd ..