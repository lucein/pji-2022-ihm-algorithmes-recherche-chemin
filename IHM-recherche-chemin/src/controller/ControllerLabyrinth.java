package controller;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import model.labyrinth.LabyrinthFullRandomize;
import model.labyrinth.LabyrinthRecursiveBacktracking;
import model.labyrinth.LabyrinthRecursiveDivision;
import resources.Resources;

/**
 * Controller class which takes care of functionalities of the labyrinth windows.
 * 
 * @author Lhomme Lucien
 */
public class ControllerLabyrinth implements Initializable {

	@FXML
	private ResourceBundle bundle;

	private Stage stage;

	private ControllerGraph graphController;

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	@FXML
	private void recursiveBacktracking() {
		graphController.generateLabyrinth(new LabyrinthRecursiveBacktracking());
		stage.close();
	}

	@FXML
	private void recursiveDivision() {
		graphController.generateLabyrinth(new LabyrinthRecursiveDivision());
		stage.close();
	}

	@FXML
	private void fullRandomize() {
		graphController.generateLabyrinth(new LabyrinthFullRandomize());
		stage.close();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		bundle = resources;
	}

	/**
	 * Change the text of all references items of FXML files with the good bundle. (If you want to add new item, you need to
	 * add the reference on the top file and add the "setText" in the method)
	 * 
	 * @param locale Locale object represents a specific geographical
	 */
	public void switchLanguage(Locale locale) {
		bundle = Resources.getBundle("strings", locale);

		stage.setTitle(bundle.getString("toolbar.createLabyrinth"));
	}

	public void injectGraphController(ControllerGraph graphController) {
		this.graphController = graphController;
	}
}
