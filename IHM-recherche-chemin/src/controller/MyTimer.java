package controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.util.Duration;
import model.Graph;
import model.Node;
import model.algorithm.Algorithm;
import model.algorithm.StepAlgo;
import model.algorithm.StepEnum;

/**
 * Class which schedule the searching algorithm and start a timeline to draw steps.
 * 
 * @author Lhomme Lucien
 */
public class MyTimer implements Runnable, Observer {

	private double refreshTime = 10;

	private Controller mainController;
	private Graph model;
	private NodeGrid nodeGrid;
	private Algorithm algo;
	private Canvas canvas;
	private ToggleButton btnLaunch;
	private HBox placableButtonsBox;
	private ObservableValue<? extends Number> sliderValueProperty;

	public MyTimer(ControllerGraph graphController, Algorithm algo, HBox placableButtonsBox) {
		this.mainController = graphController.getMainController();
		this.model = graphController.getModel();
		this.nodeGrid = graphController.getNodeGrid();
		this.algo = algo;
		this.canvas = graphController.getCanvas();
		this.btnLaunch = graphController.getBtnLaunch();
		this.placableButtonsBox = placableButtonsBox;
		this.sliderValueProperty = graphController.getSliderStepsValue();
	}

	private int lastSize = 0;
	private List<StepAlgo> list = new ArrayList<StepAlgo>();
	private List<StepAlgo> resultNodes = new ArrayList<StepAlgo>();
	private Iterator<StepAlgo> resultIte;
	private Iterator<Node> resultIteNode;
	private boolean end = false;
	private Timeline tl = null;
	private boolean drawingResult = false;
	
	@Override
	public void run() {
		nodeGrid.drawAllNodes(canvas, model);
		lastSize = 0;
		end = false;
		list.clear();
		tl = new Timeline(new KeyFrame(Duration.millis(refreshTime), ev -> {
			if (lastSize < list.size()) {
				if (list.get(lastSize).getStep().equals(StepEnum.GOOD)) {
					resultNodes.add(0, list.get(lastSize));
				} else {
					nodeGrid.drawStep(canvas, list.get(lastSize));
				}
				lastSize++;
			} else if (end) {
				stopTimeline();
			}
		}));
		//tl.rateProperty().bind(simpleDoubleProperty);
		tl.rateProperty().bind(sliderValueProperty);
		tl.setCycleCount(Animation.INDEFINITE);

		tl.play();

		Node current = nodeGrid.getBegin();
		for (Node checkpoint : nodeGrid.getCheckpoints()) {
			algo.search(model, current, checkpoint);
			current = checkpoint;
		}
		algo.search(model, current, nodeGrid.getEnd());
		/*
		 * if (nodeGrid.getCheckpoint() == null) { algo.search(model, nodeGrid.getBegin(),
		 * nodeGrid.getEnd()); } else { algo.search(model, nodeGrid.getBegin(), nodeGrid.getCheckpoint());
		 * algo.search(model, nodeGrid.getCheckpoint(), nodeGrid.getEnd()); }
		 */
		end = true;
		for (Node[] nodes : nodeGrid.getNodes()) {
			for (Node node : nodes) {
				node.setCost(0);
				node.setEstimation(0);
				node.setHeuristic(0);
			}
		}
	}

	Node lastNode = null;

	public void stopTimeline() {
		tl.pause();
		tl.stop();
		if (!drawingResult) {
			if (end == true && !resultNodes.isEmpty()) {
				resultIte = resultNodes.iterator();
				resultIteNode = resultIte.next().iterator();
				tl = new Timeline(new KeyFrame(Duration.millis(refreshTime), ev2 -> {
					GraphicsContext gc = canvas.getGraphicsContext2D();
					gc.setStroke(nodeGrid.getStepColor(StepEnum.GOOD));
					if (resultIteNode.hasNext()) {
						Node node = resultIteNode.next();
						if (lastNode != null) {
							nodeGrid.drawLink(gc, canvas.getWidth() / nodeGrid.getSize(), canvas.getHeight() / nodeGrid.getSize(), lastNode, node);
						}
						if (!node.equals(nodeGrid.getEnd()) && !nodeGrid.getCheckpoints().contains(node)) {
							nodeGrid.drawNode(gc, canvas.getWidth() / nodeGrid.getSize(), canvas.getHeight() / nodeGrid.getSize(), node);
						}
						lastNode = node;
					} else {
						if (resultIte.hasNext()) {
							resultIteNode = resultIte.next().iterator();
						} else {
							nodeGrid.drawLink(gc, canvas.getWidth() / nodeGrid.getSize(), canvas.getHeight() / nodeGrid.getSize(), lastNode,
									nodeGrid.getBegin());
							tl.pause();
							tl.stop();
							mainController.changeAction(btnLaunch, Action.NOTHING);
							for (javafx.scene.Node placableButton : placableButtonsBox.getChildren()) {
								placableButton.setDisable(false);
							}
							btnLaunch.setSelected(false);
						}
					}
				}));
				tl.rateProperty().bind(sliderValueProperty);
				tl.setCycleCount(Animation.INDEFINITE);
				tl.play();
				drawingResult = true;
				return;
			}
		}
		mainController.changeAction(btnLaunch, Action.NOTHING);
		for (javafx.scene.Node placableButton : placableButtonsBox.getChildren()) {
			placableButton.setDisable(false);
		}
		btnLaunch.setSelected(false);
	}

	@Override
	public void update(Observable o, Object arg) {
		// System.out.println("update");
		if (arg == null) {
			//
		}
		if (arg instanceof StepAlgo) {
			StepAlgo step = (StepAlgo) arg;
			list.add(step);
		}
	}

}
