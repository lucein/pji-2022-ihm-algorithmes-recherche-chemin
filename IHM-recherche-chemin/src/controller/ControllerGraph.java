package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Consumer;

import javafx.beans.property.DoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;
import model.Graph;
import model.Node;
import model.algorithm.AStar;
import model.algorithm.Algorithm;
import model.algorithm.Dijkstra;
import model.algorithm.EuclideanDistance;
import model.algorithm.EuclideanDistanceSquared;
import model.algorithm.ManhattanDistance;
import model.labyrinth.LabyrinthGenerator;
import resources.Resources;
import view.ResizableCanvas;

/**
 * Controller class which takes care of functionalities of a graph.
 * 
 * @author Lhomme Lucien
 */
public class ControllerGraph implements Initializable {

	@FXML
	private ResourceBundle bundle;

	@FXML
	private BorderPane root;

	@FXML
	private Label lblChoose;
	@FXML
	private ToggleButton btnLaunch;
	@FXML
	private Button btnClearSteps;

	@FXML
	private ChoiceBox<Algorithm> choiceBoxAlgo;

	@FXML
	private Slider sliderSteps;
	@FXML
	private Label lblSliderStepsValue;

	@FXML
	private StackPane stackPaneCanvas;

	@FXML
	private ResizableCanvas canvas;
	@FXML
	private ResizableCanvas canvasBackground;

	private MyTimer timer;
	private Thread thread;

	private Graph model;
	private NodeGrid nodeGrid;

	private Controller mainController;

	public Controller getMainController() {
		return mainController;
	}

	public Canvas getCanvas() {
		return canvas;
	}

	public Canvas getCanvasBackground() {
		return canvasBackground;
	}

	public NodeGrid getNodeGrid() {
		return nodeGrid;
	}

	public void setNodeGrid(NodeGrid nodeGrid) {
		this.nodeGrid = nodeGrid;
	}

	public Graph getModel() {
		return model;
	}

	public void setModel(Graph model) {
		this.model = model;
	}

	public ToggleButton getBtnLaunch() {
		return btnLaunch;
	}

	public DoubleProperty getSliderStepsValue() {
		return sliderSteps.valueProperty();
	}

	private List<ControllerOtherGraph> otherControllerGraph = new ArrayList<ControllerOtherGraph>();

	private void interruptLaunch() {
		if (timer != null)
			timer.stopTimeline();
		if (thread != null)
			thread.interrupt();
		mainController.changeAction(btnLaunch, Action.NOTHING);
		nodeGrid.drawAllNodes(canvas, model);
	}
	
	@FXML
	private void launch() {
		mainController.changeAction(btnLaunch, Action.RUNNING);
		if (nodeGrid.getBegin() != null && nodeGrid.getEnd() != null) {
			if (!btnLaunch.isSelected()) {
				interruptLaunch();
			} else {
				for (javafx.scene.Node placableButton : mainController.getPlacableButtonsBox().getChildren()) {
					placableButton.setDisable(true);
				}
				Algorithm algo = choiceBoxAlgo.getSelectionModel().getSelectedItem();
				algo.deleteObservers();
				timer = new MyTimer(this, choiceBoxAlgo.getSelectionModel().getSelectedItem(), mainController.getPlacableButtonsBox());
				algo.addObserver(timer);
				thread = new Thread(timer);
				thread.start();
			}
		} else {
			btnLaunch.setSelected(false);
			mainController.changeAction(btnLaunch, Action.NOTHING);
		}
		for (ControllerOtherGraph controllerOtherGraph : otherControllerGraph) {
			controllerOtherGraph.getControllerGraph().triggerLaunch(btnLaunch.isSelected());
		}
	}

	private void triggerLaunch(boolean isSelected) {
		if (btnLaunch.isSelected() == isSelected) {
			interruptLaunch();
		}
		btnLaunch.setSelected(isSelected);
		launch();
	}

	public void addOtherControllerGraph(ControllerOtherGraph controllerGraph) {
		otherControllerGraph.add(controllerGraph);
	}

	public void removeOtherControllerGraph(ControllerOtherGraph controllerGraph) {
		otherControllerGraph.remove(controllerGraph);
	}

	public int otherControllerGraphSize() {
		return otherControllerGraph.size();
	}

	public void forEachOtherControllerGraph(Consumer<? super ControllerOtherGraph> action) {
		otherControllerGraph.forEach(action);
	}

	public void deleteObstacles() {
		nodeGrid.addAllAdjNode(model);
		nodeGrid.drawAllNodes(canvas, model);
	}

	public void generateLabyrinth(LabyrinthGenerator labyrinthGenerator) {
		nodeGrid.removeAllAdjNode(model);
		labyrinthGenerator.generateLabyrinth(model, nodeGrid.getNodes());
		nodeGrid.drawAllNodes(canvas, model);
	}

	@FXML
	private void clearSteps() {
		nodeGrid.drawAllNodes(canvas, model);
	}

	private boolean placing = true;

	@FXML
	private void canvasPressed(MouseEvent event) {
		if (contextMenu != null) {
			contextMenu.hide();
		}
		if (event.isSecondaryButtonDown()) {
			return;
		}
		Node node = nodeGrid.getNodeFromCanvas(canvas, event.getX(), event.getY());
		if (node == null)
			return;
		switch (mainController.getAction()) {
		case BEGIN:
			if (node.equals(nodeGrid.getEnd())) {
				System.out.println("already used");
			} else if (nodeGrid.containsCheckpoint(node)) {
				System.out.println("already used");
			} else if (node.equals(nodeGrid.getBegin())) {
				nodeGrid.setBegin(null);
			} else if (model.getAdjNodes(node).isEmpty()) {
				System.out.println("obstacle");
			} else {
				nodeGrid.setBegin(node);
			}
			break;
		case END:
			if (node.equals(nodeGrid.getBegin())) {
				System.out.println("already used");
			} else if (nodeGrid.containsCheckpoint(node)) {
				System.out.println("already used");
			} else if (node.equals(nodeGrid.getEnd())) {
				nodeGrid.setEnd(null);
			} else if (model.getAdjNodes(node).isEmpty()) {
				System.out.println("obstacle");
			} else {
				nodeGrid.setEnd(node);
			}
			break;
		case CHECKPOINT:
			if (node.equals(nodeGrid.getBegin())) {
				System.out.println("already used");
			} else if (node.equals(nodeGrid.getEnd())) {
				System.out.println("already used");
			} else if (nodeGrid.containsCheckpoint(node)) {
				nodeGrid.removeCheckpoint(node);
			} else if (model.getAdjNodes(node).isEmpty()) {
				System.out.println("obstacle");
			} else {
				nodeGrid.addCheckpoint(node);
			}
			break;
		case OBSTACLES:
			if (model.getAdjNodes(node).isEmpty()) {
				nodeGrid.addAdjNodeIfAdjNotEmpty(model, node.getX(), node.getY());
				for (Node adj : model.getAdjNodes(node)) {
					if (!model.getAdjNodes(adj).isEmpty()) {
						model.addAdjNode(adj, node);
					}
				}
				placing = false;
			} else {
				if (node != nodeGrid.getBegin() && node != nodeGrid.getEnd()
						&& !(model.getAdjNodes(nodeGrid.getBegin()).size() == 1 && model.getAdjNodes(node).contains(nodeGrid.getBegin())
								|| model.getAdjNodes(nodeGrid.getEnd()).size() == 1 && model.getAdjNodes(node).contains(nodeGrid.getEnd()))) {
					model.removeAdjNode(node);
				}
				placing = true;
			}
			break;
		default:
			return;
		}
		nodeGrid.drawAllNodes(canvas, model);
	}

	@FXML
	private void canvasDragged(MouseEvent event) {
		Node node = nodeGrid.getNodeFromCanvas(canvas, event.getX(), event.getY());
		if (node == null)
			return;
		switch (mainController.getAction()) {
		case OBSTACLES:
			if (!placing) {
				nodeGrid.addAdjNodeIfAdjNotEmpty(model, node.getX(), node.getY());
				for (Node adj : model.getAdjNodes(node)) {
					if (!model.getAdjNodes(adj).isEmpty()) {
						model.addAdjNode(adj, node);
					}
				}
			} else {
				if (node != nodeGrid.getBegin() && node != nodeGrid.getEnd()
						&& !(model.getAdjNodes(nodeGrid.getBegin()).size() == 1 && model.getAdjNodes(node).contains(nodeGrid.getBegin())
								|| model.getAdjNodes(nodeGrid.getEnd()).size() == 1 && model.getAdjNodes(node).contains(nodeGrid.getEnd()))) {
					model.removeAdjNode(node);
				}
			}
			// nodeGrid.drawNodeBackground(canvas, model, node.getX(), node.getY());
			break;
		default:
			return;
		}
		nodeGrid.drawAllNodes(canvas, model);
	}

	private static ContextMenu contextMenu;

	@FXML
	private void canvasContextMenu(ContextMenuEvent event) {
		if (contextMenu == null) {
			contextMenu = new ContextMenu();
			MenuItem menuItem = new MenuItem("nouvelle fen�tre", new ImageView(Resources.getImage("OpenNewWindow.png")));
			menuItem.setOnAction(e -> {
				try {
					mainController.newWindow();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			});
			contextMenu.getItems().add(menuItem);
		} else {
			contextMenu.hide();
		}
		contextMenu.show(canvas, event.getScreenX(), event.getScreenY());
	}
	
	public void resizeGraph(int goal) {
		if (goal < nodeGrid.getSize()) {
			for (int i = nodeGrid.getSize(); i > goal; i--) {
				decreaseGraphSize();
			}
		} else {
			for (int i = nodeGrid.getSize(); i < goal; i++) {
				increaseGraphSize();
			}
		}
	}
	
	private void increaseGraphSize() {
		nodeGrid.realocGrid(nodeGrid.getSize() + 1);
		for (int i = 0; i < nodeGrid.getSize(); i++) {
			model.addAdjNode(nodeGrid.getNode(i, nodeGrid.getSize() - 2), nodeGrid.getNode(i, nodeGrid.getSize() - 1));
			nodeGrid.addAdjNode(model, i, nodeGrid.getSize() - 1);
		}
		for (int j = 0; j < nodeGrid.getSize() - 1; j++) {
			model.addAdjNode(nodeGrid.getNode(nodeGrid.getSize() - 2, j), nodeGrid.getNode(nodeGrid.getSize() - 1, j));
			nodeGrid.addAdjNode(model, nodeGrid.getSize() - 1, j);
		}
	}
	
	private Node checkIfSpecialNodeWillBeOutOfArray(Node node) {
		if (node != null && (node.getX() == nodeGrid.getSize()-1 || node.getY() == nodeGrid.getSize()-1)) {
			return null;
		}
		return node;
	}
	
	private void decreaseGraphSize() {
		if (nodeGrid.getSize() == 2) {
			return;
		}
		nodeGrid.setBegin(checkIfSpecialNodeWillBeOutOfArray(nodeGrid.getBegin()));
		for (int i = 0; i < nodeGrid.getCheckpoints().size(); i++) {
			if (checkIfSpecialNodeWillBeOutOfArray(nodeGrid.getCheckpoints().get(i)) == null) {
				nodeGrid.getCheckpoints().remove(nodeGrid.getCheckpoints().get(i));
				i--;
			}
		}
		nodeGrid.setEnd(checkIfSpecialNodeWillBeOutOfArray(nodeGrid.getEnd()));
		for (int i = 0; i < nodeGrid.getSize(); i++) {
			model.removeAdjNode(nodeGrid.getNode(i, nodeGrid.getSize() - 1));
		}
		for (int j = 0; j < nodeGrid.getSize() - 1; j++) {
			model.removeAdjNode(nodeGrid.getNode(nodeGrid.getSize() - 1, j));
		}
		nodeGrid.realocGrid(nodeGrid.getSize() - 1);
	}

	@FXML
	private void canvasScroll(ScrollEvent event) {
		if (mainController.getAction() == Action.RUNNING) {
			return;
		}
		if (event.getDeltaY() > 0) {
			decreaseGraphSize();
		} else {
			increaseGraphSize();
		}
		nodeGrid.drawAllBackGround(canvasBackground, model);
		nodeGrid.drawAllNodes(canvas, model);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		bundle = resources;

		Algorithm defaultAlgorithm;
		// ObservableList<Algorithm> algorithms = FXCollections.observableArrayList(defaultAlgorithm = new
		// Dijkstra(), new AStar(new HeuristicNearest()),
		// new AStar(new HeuristicBetter())/* , placer vos algorithms i�i */);
		ObservableList<Algorithm> algorithms = FXCollections.observableArrayList(defaultAlgorithm = new Dijkstra(), new AStar(new EuclideanDistance()),
				new AStar(new EuclideanDistanceSquared()), new AStar(new ManhattanDistance())/* , placer vos algorithms i�i */);
		choiceBoxAlgo.setConverter(new StringConverter<Algorithm>() {
			@Override
			public Algorithm fromString(String string) {
				return null;
			}

			@Override
			public String toString(Algorithm algo) {
				return algo.getName();
			}
		});
		choiceBoxAlgo.setItems(algorithms);
		choiceBoxAlgo.setValue(defaultAlgorithm);
		model = new Graph();
		nodeGrid = new NodeGrid(20);
		nodeGrid.addAllAdjNode(model);

		BorderPane.setAlignment(stackPaneCanvas, Pos.CENTER);

		canvas.setDraw((canvas, graph) -> nodeGrid.drawAllNodes(canvas, graph), model);
		canvasBackground.setDraw((canvas, graph) -> nodeGrid.drawAllBackGround(canvas, graph), model);

		nodeGrid.drawAllBackGround(canvasBackground, model);
		nodeGrid.drawAllNodes(canvas, model);

		sliderSteps.valueProperty().addListener((observer, oldValue, newValue) -> lblSliderStepsValue.setText(String.format("x%.2f", newValue.doubleValue())));
	}

	/**
	 * Change the text of all references items of FXML files with the good bundle. (If you want to add new item, you need to
	 * add the reference on the top file and add the "setText" in the method)
	 * 
	 * @param locale Locale object represents a specific geographical
	 */
	public void switchLanguage(Locale locale) {
		bundle = Resources.getBundle("strings", locale);

		lblChoose.setText(bundle.getString("toolbar.chooseAlgo"));
		btnLaunch.setText(bundle.getString("toolbar.launch"));
		btnClearSteps.setText(bundle.getString("toolbar.clearSteps"));
	}

	public void injectMainController(Controller mainController) {
		this.mainController = mainController;
	}

}
