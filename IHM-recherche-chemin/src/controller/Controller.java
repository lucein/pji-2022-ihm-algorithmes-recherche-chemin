package controller;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Graph;
import resources.Resources;

/**
 * Controller class which takes care of functionalities.
 * 
 * @author Lhomme Lucien
 */
public class Controller implements Initializable {

	@FXML
	private ResourceBundle bundle;

	@FXML
	private MenuBar menuBar;
	@FXML
	private Menu menuFile;
	@FXML
	private MenuItem menuClose;
	@FXML
	private MenuItem menuQuit;

	@FXML
	private Menu menuEdit;
	@FXML
	private MenuItem menuDelete;

	@FXML
	private Menu menuWindow;
	@FXML
	private MenuItem menuNewWindow;
	@FXML
	private MenuItem menuFullScreen;
	@FXML
	private ImageView imageViewFullScreen;
	@FXML
	private Menu menuLanguages;
	@FXML
	private MenuItem menuEnglish;
	@FXML
	private MenuItem menuFrench;

	@FXML
	private Menu menuHelp;
	@FXML
	private MenuItem menuAbout;

	@FXML
	private VBox root;
	@FXML
	private ToolBar toolBar;

	@FXML
	private Label lblPlace;
	@FXML
	private ToggleButton btnBegin;
	@FXML
	private ToggleButton btnEnd;
	@FXML
	private ToggleButton btnCheckpoint;
	@FXML
	private ToggleButton btnObstacles;
	@FXML
	private ToggleButton btnCreateLabyrinth;
	@FXML
	private Button btnDeleteObstacles;

	@FXML
	private HBox placableButtonsBox;

	@FXML
	private Button btnNewWindow;

	@FXML
	private Label lblAction;
	@FXML
	private Label lblActionValue;

	private Stage primaryStage;

	private Action action = Action.NOTHING;

	@FXML
	private BorderPane graphView;
	@FXML
	private ControllerGraph graphViewController;

	/**
	 * Give the reference of the primary stage.
	 * 
	 * @param primaryStage Stage
	 */
	public void setStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public Stage getStage() {
		return primaryStage;
	}

	/**
	 * Called method when the user click on the "quit" button.
	 */
	@FXML
	private void quit() {
		System.exit(0);
	}

	@FXML
	private void suppr() {

	}

	@FXML
	private void about() {

	}

	@FXML
	public void fullScreen() {
		primaryStage.setFullScreen(!primaryStage.isFullScreen());
	}

	@FXML
	private void begin() {
		changeAction(btnBegin, Action.BEGIN);
	}

	@FXML
	private void end() {
		changeAction(btnEnd, Action.END);
	}

	@FXML
	private void checkpoint() {
		changeAction(btnCheckpoint, Action.CHECKPOINT);
	}

	@FXML
	private void obstacles() {
		changeAction(btnObstacles, Action.OBSTACLES);
	}

	public Action getAction() {
		return action;
	}

	public void changeAction(ToggleButton toggleButton, Action action) {
		this.action = action;
		for (Node node : placableButtonsBox.getChildren()) {
			if (!node.equals(toggleButton) && node instanceof ToggleButton) {
				ToggleButton placableButton = (ToggleButton) node;
				placableButton.setSelected(false);
			}
		}
		lblActionValue.setText(getActionText(bundle, action));
	}

	private String getActionText(ResourceBundle bundle, Action action) {
		return bundle.getString(String.format("toolbar.%s", action.getText()));
	}

	public HBox getPlacableButtonsBox() {
		return placableButtonsBox;
	}

	private ControllerLabyrinth controllerLabyrinth;

	@FXML
	private void createLabyrinth() throws IOException {
		if (controllerLabyrinth == null) {
			Stage stage3 = new Stage();
			stage3.initOwner(primaryStage);
	
			FXMLLoader fxmlLoader = new FXMLLoader(Resources.getURL("views/MyViewLabyrinth.fxml"), bundle);
			Parent root = fxmlLoader.load();
	
			controllerLabyrinth = (ControllerLabyrinth) fxmlLoader.getController();
			controllerLabyrinth.injectGraphController(graphViewController);
			controllerLabyrinth.setStage(stage3);
	
			Scene scene = new Scene(root);
			scene.getStylesheets().add(Resources.getURL("views/application.css").toExternalForm());
	
			stage3.getIcons().addAll(primaryStage.getIcons());
			stage3.setTitle(bundle.getString("toolbar.createLabyrinth"));
			stage3.setScene(scene);
		}
		controllerLabyrinth.getStage().show();
	}

	@FXML
	private void deleteObstacles() {
		graphViewController.deleteObstacles();
	}

	@FXML
	public void newWindow() throws IOException {
		Stage stage2 = new Stage();
		stage2.initModality(Modality.NONE);
		stage2.initOwner(primaryStage);

		FXMLLoader fxmlLoader = new FXMLLoader(Resources.getURL("views/MyViewOtherGraph.fxml"), bundle);
		ControllerOtherGraph controllerOtherGraph = new ControllerOtherGraph(stage2, 2 + this.graphViewController.otherControllerGraphSize());
		fxmlLoader.setController(controllerOtherGraph);
		Parent root = fxmlLoader.load();

		ControllerGraph controllerGraph = controllerOtherGraph.getControllerGraph();
		controllerGraph.injectMainController(this);
		controllerGraph.getSliderStepsValue().bindBidirectional(graphViewController.getSliderStepsValue());

		NodeGrid nodeGrid2 = graphViewController.getNodeGrid().copy();
		Graph model2 = controllerGraph.getModel();
		nodeGrid2.addAllAdjNodeFromTo(graphViewController.getModel(), model2);
		controllerGraph.setNodeGrid(nodeGrid2);

		Canvas canvasBackground2 = controllerGraph.getCanvasBackground();
		Canvas canvas2 = controllerGraph.getCanvas();
		nodeGrid2.drawAllBackGround(canvasBackground2, model2);
		nodeGrid2.drawAllNodes(canvas2, model2);

		Scene scene = new Scene(root);
		scene.getStylesheets().add(Resources.getURL("views/application.css").toExternalForm());

		stage2.getIcons().addAll(primaryStage.getIcons());
		stage2.setTitle(String.format("%s (%d)", primaryStage.getTitle(), 2 + this.graphViewController.otherControllerGraphSize()));
		stage2.setScene(scene);
		stage2.show();

		this.graphViewController.addOtherControllerGraph(controllerOtherGraph);

		stage2.setOnCloseRequest(ev -> this.graphViewController.removeOtherControllerGraph(controllerOtherGraph));
	}

	@FXML
	private void switchLanguageEN() {
		switchLanguage(Locale.ENGLISH);
	}

	@FXML
	private void switchLanguageFR() {
		switchLanguage(Locale.FRENCH);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		bundle = resources;
		menuBar.setUseSystemMenuBar(true);

		graphViewController.injectMainController(this);

		root.setAlignment(Pos.CENTER);
	}

	/**
	 * Change the text of all references items of FXML files with the good bundle. (If you want to add new item, you need to
	 * add the reference on the top file and add the "setText" in the method)
	 * 
	 * @param locale Locale object represents a specific geographical
	 */
	private void switchLanguage(Locale locale) {
		bundle = Resources.getBundle("strings", locale);

		primaryStage.setTitle(bundle.getString("window.title"));

		menuFile.setText(bundle.getString("menu.file"));
		menuClose.setText(bundle.getString("menu.close"));
		menuQuit.setText(bundle.getString("menu.quit"));

		menuEdit.setText(bundle.getString("menu.edit"));
		menuDelete.setText(bundle.getString("menu.delete"));

		menuWindow.setText(bundle.getString("menu.window"));
		menuNewWindow.setText(bundle.getString("toolbar.newWindow"));
		menuFullScreen.setText(bundle.getString("menu.fullScreen"));
		menuLanguages.setText(bundle.getString("menu.languages"));
		menuEnglish.setText(bundle.getString("menu.english"));
		menuFrench.setText(bundle.getString("menu.french"));

		menuHelp.setText(bundle.getString("menu.help"));
		menuAbout.setText(bundle.getString("menu.about"));

		lblPlace.setText(bundle.getString("toolbar.place"));
		btnBegin.setText(bundle.getString("toolbar.begin"));
		btnEnd.setText(bundle.getString("toolbar.end"));
		btnCheckpoint.setText(bundle.getString("toolbar.checkpoint"));
		btnObstacles.setText(bundle.getString("toolbar.obstacles"));
		btnCreateLabyrinth.setText(bundle.getString("toolbar.createLabyrinth"));
		btnDeleteObstacles.setText(bundle.getString("toolbar.deleteObstacles"));
		btnNewWindow.setText(bundle.getString("toolbar.newWindow"));

		lblActionValue.setText(getActionText(bundle, action));

		if (controllerLabyrinth != null) {
			controllerLabyrinth.switchLanguage(locale);
		}
		graphViewController.switchLanguage(locale);
		graphViewController.forEachOtherControllerGraph((ControllerOtherGraph controllerOtherGraph) -> controllerOtherGraph.switchLanguage(locale));
	}

	public ControllerGraph getGraphViewController() {
		return graphViewController;
	}

}
