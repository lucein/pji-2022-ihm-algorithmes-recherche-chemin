package controller;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Graph;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import resources.Resources;

/**
 * Controller class which takes care of functionalities of an other Graph window.
 * 
 * @author Lhomme Lucien
 */
public class ControllerOtherGraph implements Initializable {

	public ControllerOtherGraph(Stage stage, int numberStage) {
		this.stage = stage;
		this.numberStage = numberStage;
	}

	@FXML
	private ResourceBundle bundle;

	@FXML
	private VBox root;
	@FXML
	private ToolBar toolBarGraph;

	@FXML
	private Button btnSynchronize;

	@FXML
	private BorderPane graphView;
	@FXML
	private StackPane stackPaneCanvas;

	@FXML
	private ControllerGraph graphViewController;

	private Stage stage;
	private int numberStage;

	public void setStage(Stage stage, int numberStage) {
		this.stage = stage;
		this.numberStage = numberStage;
	}

	@FXML
	private void synchronize() {
		ControllerGraph mainGraphViewController = graphViewController.getMainController().getGraphViewController();
		NodeGrid nodeGrid = graphViewController.getNodeGrid();
		Graph model = graphViewController.getModel();
		
		graphViewController.resizeGraph(mainGraphViewController.getNodeGrid().getSize());
		
		mainGraphViewController.getNodeGrid().copySpecialsNodesPositions(graphViewController.getNodeGrid());

		nodeGrid.removeAllAdjNode(model);
		nodeGrid.addAllAdjNodeFromTo(mainGraphViewController.getModel(), model);
		nodeGrid.drawAllBackGround(graphViewController.getCanvasBackground(), model);
		nodeGrid.drawAllNodes(graphViewController.getCanvas(), model);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		bundle = resources;
	}

	/**
	 * Change the text of all references items of FXML files with the good bundle. (If you want to add new item, you need to
	 * add the reference on the top file and add the "setText" in the method)
	 * 
	 * @param locale Locale object represents a specific geographical
	 */
	public void switchLanguage(Locale locale) {
		bundle = Resources.getBundle("strings", locale);

		stage.setTitle(String.format("%s (%d)", bundle.getString("window.title"), numberStage));

		btnSynchronize.setText(bundle.getString("toolbar.synchronize"));

		graphViewController.switchLanguage(locale);
	}

	public ControllerGraph getControllerGraph() {
		return graphViewController;
	}

}
