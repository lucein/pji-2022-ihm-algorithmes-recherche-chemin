package controller;

public enum Action {

	NOTHING("actionNothing"), BEGIN("begin"), END("end"), CHECKPOINT("checkpoint"), OBSTACLES("obstacles"), RUNNING("actionRunning");
	
	private String text;
	
	private Action(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

}
