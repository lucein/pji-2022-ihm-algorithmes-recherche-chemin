package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.Graph;
import model.Node;
import model.algorithm.StepAlgo;
import model.algorithm.StepEnum;

/**
 * Class which memorize all nodes to draw and how to display them.
 * 
 * @author Lhomme Lucien
 */
public class NodeGrid {

	private int size;
	private Node[][] nodes;

	private Node begin = null;

	private Node end = null;

	private List<Node> checkpoints;

	public NodeGrid(int size) {
		this.size = size;
		this.nodes = new Node[size][size];

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				nodes[i][j] = new Node(i, j);
			}
		}

		checkpoints = new ArrayList<Node>();

		begin = nodes[1][size - 2];

		end = nodes[size - 2][1];
	}

	public int getSize() {
		return size;
	}

	public void realocGrid(int size) {
		Node[][] tmp = new Node[size][size];

		int i = 0;
		for (; i < Math.min(this.size, size); i++) {
			int j = 0;
			for (; j < Math.min(this.size, size); j++) {
				tmp[i][j] = nodes[i][j];
			}
			for (; j < size; j++) {
				tmp[i][j] = new Node(i, j);
			}
		}
		for (; i < size; i++) {
			for (int j = 0; j < size; j++) {
				tmp[i][j] = new Node(i, j);
			}
		}
		this.size = size;
		this.nodes = tmp;
	}

	public void setBegin(Node begin) {
		this.begin = begin;
	}

	public Node getBegin() {
		return begin;
	}

	public void setEnd(Node end) {
		this.end = end;
	}

	public Node getEnd() {
		return end;
	}

	public void addCheckpoint(Node checkpoint) {
		checkpoints.add(checkpoint);
	}

	public void removeCheckpoint(Node checkpoint) {
		checkpoints.remove(checkpoint);
	}

	public boolean containsCheckpoint(Node checkpoint) {
		return checkpoints.contains(checkpoint);
	}

	public List<Node> getCheckpoints() {
		return checkpoints;
	}

	public void addAdjNodeIfAdjNotEmpty(Graph graph, int i, int j) {
		if (j > 0 && !graph.getAdjNodes(nodes[i][j - 1]).isEmpty())
			graph.addAdjNode(nodes[i][j], nodes[i][j - 1]);
		if (i < size - 1 && !graph.getAdjNodes(nodes[i + 1][j]).isEmpty())
			graph.addAdjNode(nodes[i][j], nodes[i + 1][j]);
		if (j < size - 1 && !graph.getAdjNodes(nodes[i][j + 1]).isEmpty())
			graph.addAdjNode(nodes[i][j], nodes[i][j + 1]);
		if (i > 0 && !graph.getAdjNodes(nodes[i - 1][j]).isEmpty())
			graph.addAdjNode(nodes[i][j], nodes[i - 1][j]);
	}

	public void addAdjNode(Graph graph, int i, int j) {
		if (j > 0)
			graph.addAdjNode(nodes[i][j], nodes[i][j - 1]);
		if (i < size - 1)
			graph.addAdjNode(nodes[i][j], nodes[i + 1][j]);
		if (j < size - 1)
			graph.addAdjNode(nodes[i][j], nodes[i][j + 1]);
		if (i > 0)
			graph.addAdjNode(nodes[i][j], nodes[i - 1][j]);
	}

	public void addAllAdjNode(Graph graph) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				addAdjNode(graph, i, j);
			}
		}

		// --- Tests ---
		/*
		 * graph.getAdjNodes(nodes[0][0]).remove(nodes[1][0]); graph.getAdjNodes(nodes[1][0]).remove(nodes[0][0]);
		 * 
		 * graph.getAdjNodes(nodes[0][1]).remove(nodes[1][1]); graph.getAdjNodes(nodes[1][1]).remove(nodes[0][1]);
		 * 
		 * graph.getAdjNodes(nodes[1][2]).remove(nodes[1][3]); graph.getAdjNodes(nodes[1][3]).remove(nodes[1][2]);
		 * 
		 * graph.getAdjNodes(nodes[0][3]).remove(nodes[0][4]); graph.getAdjNodes(nodes[0][4]).remove(nodes[0][3]);
		 * 
		 * graph.getAdjNodes(nodes[1][3]).remove(nodes[1][4]); graph.getAdjNodes(nodes[1][4]).remove(nodes[1][3]);
		 */
	}

	public void removeAllAdjNode(Graph graph) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				graph.removeAdjNode(nodes[i][j]);
			}
		}
	}

	public void addAllAdjNodeFromTo(Graph from, Graph to) {
		for (Entry<Node, List<Node>> entry1 : from.entrySet()) {
			for (Node node : entry1.getValue()) {
				to.addAdjNode(nodes[entry1.getKey().getX()][entry1.getKey().getY()], nodes[node.getX()][node.getY()]);
			}
		}
	}

	public void drawNodeBackground(GraphicsContext gc, double width, double height, int i, int j) {
		if ((i + j) % 2 == 0) {
			gc.setFill(Color.ALICEBLUE);
		} else {
			gc.setFill(Color.WHITE);
		}
		gc.fillRect(i * width, j * height, width, height);
	}

	private void drawObstaclesLeftUp(GraphicsContext gc, double width, double height, Graph graph, int i, int j) {
		if (graph.getAdjNodes(nodes[i][j]).isEmpty()) {
			gc.fillRect(i * width, j * height, width, height);
		}
		if (i > 0) {
			if (!graph.getAdjNodes(nodes[i][j]).contains(nodes[i - 1][j]) && !graph.getAdjNodes(nodes[i][j]).isEmpty()
					|| graph.getAdjNodes(nodes[i][j]).isEmpty() != graph.getAdjNodes(nodes[i - 1][j]).isEmpty()) {
				gc.strokeLine(i * width, j * height + 1, i * width, (j + 1) * height - 1);
			}
		}
		if (j > 0) {
			if (!graph.getAdjNodes(nodes[i][j]).contains(nodes[i][j - 1]) && !graph.getAdjNodes(nodes[i][j]).isEmpty()
					|| graph.getAdjNodes(nodes[i][j]).isEmpty() != graph.getAdjNodes(nodes[i][j - 1]).isEmpty()) {
				gc.strokeLine(i * width + 1, j * height, (i + 1) * width - 1, j * height);
			}
		}
	}

	@SuppressWarnings("unused")
	private void drawObstaclesInner(Canvas canvas, Graph graph, int i, int j) {
		GraphicsContext gc = canvas.getGraphicsContext2D();
		if (i > 0) {
			/*
			 * if (!graph.getAdjNodes(nodes[i][j]).contains(nodes[i - 1][j])) { gc.setLineWidth(2); gc.setStroke(Color.BLACK);
			 * gc.strokeLine(i * (canvas.getWidth() / SIZE)+1, j * (canvas.getHeight() / SIZE)+1, i * (canvas.getWidth() / SIZE)+1,
			 * (j + 1) * (canvas.getHeight() / SIZE)-1); }
			 */
			gc.setLineWidth(1);
			gc.setStroke(Color.BLACK);
			if (graph.getAdjNodes(nodes[i][j]).isEmpty()) {
				if (!graph.getAdjNodes(nodes[i - 1][j]).isEmpty()) {
					gc.strokeLine(i * (canvas.getWidth() / size), j * (canvas.getHeight() / size), i * (canvas.getWidth() / size),
							(j + 1) * (canvas.getHeight() / size) - 1);
				}
			} else {
				if (!graph.getAdjNodes(nodes[i - 1][j]).isEmpty() && !graph.getAdjNodes(nodes[i][j]).contains(nodes[i - 1][j])) {
					gc.strokeLine(i * (canvas.getWidth() / size), j * (canvas.getHeight() / size), i * (canvas.getWidth() / size),
							(j + 1) * (canvas.getHeight() / size) - 1);
				}
			}
		}
		if (j > 0) {
			/*
			 * if (!graph.getAdjNodes(nodes[i][j]).contains(nodes[i][j - 1])) { gc.setLineWidth(2); gc.setStroke(Color.BLACK);
			 * gc.strokeLine(i * (canvas.getWidth() / SIZE)+1, j * (canvas.getHeight() / SIZE)+1, (i + 1) * (canvas.getWidth() /
			 * SIZE)-1, j * (canvas.getHeight() / SIZE)+1); }
			 */
			gc.setLineWidth(1);
			gc.setStroke(Color.BLACK);
			if (graph.getAdjNodes(nodes[i][j]).isEmpty()) {
				if (!graph.getAdjNodes(nodes[i][j - 1]).isEmpty()) {
					gc.strokeLine(i * (canvas.getWidth() / size), j * (canvas.getHeight() / size), (i + 1) * (canvas.getWidth() / size) - 1,
							j * (canvas.getHeight() / size));
				}
			} else {
				if (!graph.getAdjNodes(nodes[i][j - 1]).isEmpty() && !graph.getAdjNodes(nodes[i][j]).contains(nodes[i][j - 1])) {
					gc.strokeLine(i * (canvas.getWidth() / size), j * (canvas.getHeight() / size), (i + 1) * (canvas.getWidth() / size) - 1,
							j * (canvas.getHeight() / size));
				}
			}
		}
		if (i < size - 1) {
			gc.setLineWidth(1);
			gc.setStroke(Color.BLACK);
			/*
			 * if (graph.getAdjNodes(nodes[i][j]).isEmpty()) { if (graph.getAdjNodes(nodes[i+1][j]).isEmpty()) { gc.strokeLine((i+1)
			 * * (canvas.getWidth() / SIZE)-1, j * (canvas.getHeight() / SIZE)+1, (i+1) * (canvas.getWidth() / SIZE)-1, (j + 1) *
			 * (canvas.getHeight() / SIZE)-1); } else { gc.strokeLine((i+1) * (canvas.getWidth() / SIZE)-1, j * (canvas.getHeight()
			 * / SIZE)+1, (i+1) * (canvas.getWidth() / SIZE)-1, (j + 1) * (canvas.getHeight() / SIZE)-1); } }
			 */
			if (graph.getAdjNodes(nodes[i][j]).isEmpty()) {
				if (!graph.getAdjNodes(nodes[i + 1][j]).isEmpty()) {
					gc.strokeLine((i + 1) * (canvas.getWidth() / size) - 1, j * (canvas.getHeight() / size), (i + 1) * (canvas.getWidth() / size) - 1,
							(j + 1) * (canvas.getHeight() / size) - 1);
				}
			} else {
				if (!graph.getAdjNodes(nodes[i + 1][j]).isEmpty() && !graph.getAdjNodes(nodes[i][j]).contains(nodes[i + 1][j])) {
					gc.strokeLine((i + 1) * (canvas.getWidth() / size) - 1, j * (canvas.getHeight() / size), (i + 1) * (canvas.getWidth() / size) - 1,
							(j + 1) * (canvas.getHeight() / size) - 1);
				}
			}
		}
		if (j < size - 1) {
			gc.setLineWidth(1);
			gc.setStroke(Color.BLACK);
			/*
			 * if (graph.getAdjNodes(nodes[i][j]).isEmpty()) { if (graph.getAdjNodes(nodes[i][j+1]).isEmpty()) { if
			 * (!graph.getAdjNodes(nodes[i+1][j]).isEmpty()) { gc.strokeLine((i+1) * (canvas.getWidth() / SIZE)-1, j *
			 * (canvas.getHeight() / SIZE)+1, (i+1) * (canvas.getWidth() / SIZE)-1, (j + 1) * (canvas.getHeight() / SIZE)); } if
			 * (!graph.getAdjNodes(nodes[i-1][j]).isEmpty()) { gc.strokeLine(i * (canvas.getWidth() / SIZE)+1, j *
			 * (canvas.getHeight() / SIZE)+1, i * (canvas.getWidth() / SIZE)+1, (j + 1) * (canvas.getHeight() / SIZE)); } } else {
			 * gc.strokeLine(i * (canvas.getWidth() / SIZE)+1, (j + 1) * (canvas.getHeight() / SIZE)-1, (i+1) * (canvas.getWidth() /
			 * SIZE)-1, (j + 1) * (canvas.getHeight() / SIZE)-1); } }
			 */
			if (graph.getAdjNodes(nodes[i][j]).isEmpty()) {
				if (!graph.getAdjNodes(nodes[i][j + 1]).isEmpty()) {
					gc.strokeLine(i * (canvas.getWidth() / size), (j + 1) * (canvas.getHeight() / size) - 1, (i + 1) * (canvas.getWidth() / size) - 1,
							(j + 1) * (canvas.getHeight() / size) - 1);
				}
			} else {
				if (!graph.getAdjNodes(nodes[i][j + 1]).isEmpty() && !graph.getAdjNodes(nodes[i][j]).contains(nodes[i][j + 1])) {
					gc.strokeLine(i * (canvas.getWidth() / size), (j + 1) * (canvas.getHeight() / size) - 1, (i + 1) * (canvas.getWidth() / size) - 1,
							(j + 1) * (canvas.getHeight() / size) - 1);
				}
			}
		}
	}

	public void drawAllBackGround(Canvas canvas, Graph graph) {
		GraphicsContext gc = canvas.getGraphicsContext2D();
		double width = canvas.getWidth() / size;
		double height = canvas.getHeight() / size;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				drawNodeBackground(gc, width, height, i, j);
			}
		}
	}

	private static final Color CHECKPOINT_COLOR = Color.rgb(32, 178, 204);
	
	public void drawAllNodes(Canvas canvas, Graph graph) {
		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		gc.setLineWidth(2);
		gc.setStroke(Color.BLACK);
		gc.setFill(Color.LIGHTGRAY);
		double width = canvas.getWidth() / size;
		double height = canvas.getHeight() / size;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				drawObstaclesLeftUp(gc, width, height, graph, i, j);
			}
		}
		if (begin != null) {
			gc.setStroke(Color.LIMEGREEN);
			drawNode(gc, width, height, begin);
		}
		if (end != null) {
			gc.setStroke(Color.RED);
			drawNode(gc, width, height, end);
		}
		for (Node checkpoint : checkpoints) {
			gc.setStroke(CHECKPOINT_COLOR);
			drawNode(gc, width, height, checkpoint);
		}
		gc.setLineWidth(2);
		gc.setStroke(Color.BLACK);
		gc.strokeRect(0, 0, canvas.getWidth(), canvas.getHeight());
	}

	public void drawNode(GraphicsContext gc, double width, double height, Node node) {
		// drawNodeAsRect(gc, width, height, node);
		drawNodeAsOval(gc, width, height, node);
	}

	@SuppressWarnings("unused")
	private void drawNodeAsRect(GraphicsContext gc, double width, double height, Node node) {
		// gc.clearRect(node.getX() * width + 2, node.getY() * height + 2, width - 4, height - 4);
		gc.strokeRect(node.getX() * width + 2, node.getY() * height + 2, width - 4, height - 4);
		// System.out.println(node.getHeuristic());
		// gc.strokeText(""+node.getHeuristic(), node.getX() * width + 4, node.getY() * height + height - 5);
	}

	private void drawNodeAsOval(GraphicsContext gc, double width, double height, Node node) {
		gc.strokeOval(node.getX() * width + 3, node.getY() * height + 3, width - 6, height - 6);
	}

	private static final Color PASSED_COLOR = Color.web("0xa46a51");

	public Color getStepColor(StepEnum stepEnum) {
		switch (stepEnum) {
		case CHECKED:
			return Color.ORANGE;
		case PASSED:
			// return Color.ROSYBROWN;
			// return Color.SADDLEBROWN;
			return PASSED_COLOR;
		case GOOD:
			return Color.BLUE;
		default:
			return null;
		}
	}

	public void drawStep(Canvas canvas, StepAlgo step) {
		GraphicsContext gc = canvas.getGraphicsContext2D();
		double width = canvas.getWidth() / size;
		double height = canvas.getHeight() / size;
		gc.setStroke(getStepColor(step.getStep()));
		for (Node node : step) {
			if (node != end && node != begin && !checkpoints.contains(node)) {
				drawNode(gc, width, height, node);
				if (step.getOrigin() != null) {
					drawLink(gc, width, height, step.getOrigin(), node);
				}
			}
		}
	}

	public void drawLink(GraphicsContext gc, double width, double height, Node node1, Node node2) {
		if (node1.getX() > node2.getX()) {
			gc.strokeLine(node1.getX() * width + 1, node1.getY() * height + height / 2, node1.getX() * width - 1, node1.getY() * height + height / 2);
		} else if (node1.getX() < node2.getX()) {
			gc.strokeLine(node2.getX() * width + 1, node2.getY() * height + height / 2, node2.getX() * width - 1, node2.getY() * height + height / 2);
		} else if (node1.getY() > node2.getY()) {
			gc.strokeLine(node1.getX() * width + width / 2, node1.getY() * height + 1, node1.getX() * width + width / 2, node1.getY() * height - 1);
		} else if (node1.getY() < node2.getY()) {
			gc.strokeLine(node2.getX() * width + width / 2, node2.getY() * height + 1, node2.getX() * width + width / 2, node2.getY() * height - 1);
		}
	}

	/**
	 * Get the specific node from canvas sizes, null if out of array.
	 * 
	 * @param x integer for the first dimension.
	 * @param y integer for the second dimension.
	 * @return the specific node if in array, null if not.
	 */
	public Node getNodeFromCanvas(Canvas canvas, double x, double y) {
		int i = (int) (x / canvas.getWidth() * size);
		int j = (int) (y / canvas.getHeight() * size);
		// System.out.println(i + " " + j);
		return getNode(i, j);
	}

	public void copySpecialsNodesPositions(NodeGrid clone) {
		if (this.begin != null) {
			clone.begin = clone.nodes[this.begin.getX()][this.begin.getY()];
		}
		if (this.end != null) {
			clone.end = clone.nodes[this.end.getX()][this.end.getY()];
		}
		for (Node checkpoint : this.checkpoints) {
			clone.addCheckpoint(clone.nodes[checkpoint.getX()][checkpoint.getY()]);
		}
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		NodeGrid clone = new NodeGrid(size);

		copySpecialsNodesPositions(clone);

		return clone;
	}

	public NodeGrid copy() {
		try {
			return (NodeGrid) clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Get the specific node, null if out of array.
	 * 
	 * @param i integer for the first dimension.
	 * @param j integer for the second dimension.
	 * @return the specific node if in array, null if not.
	 */
	public Node getNode(int i, int j) {
		if (i >= 0 && i < size && j >= 0 && j < size) {
			return nodes[i][j];
		}
		return null;
	}
	
	public Node[][] getNodes() {
		return nodes;
	}

}
