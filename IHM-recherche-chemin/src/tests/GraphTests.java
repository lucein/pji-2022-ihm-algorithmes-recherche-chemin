package tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import model.Graph;
import model.Node;
import model.algorithm.AStar;
import model.algorithm.Dijkstra;
import model.algorithm.EuclideanDistance;
//import model.algorithm.HeuristicNearest;

public class GraphTests {

	@Test
	public void testPlusCourtCheminDijkstra() {
		Graph graph = new Graph();
		
		Node node1 = new Node(0, 0);
		Node node2 = new Node(1, 0);
		Node node3 = new Node(2, 0);
		Node node4 = new Node(3, 0);
		graph.addAdjNode(node1, node2);
		graph.addAdjNode(node2, node1);
		graph.addAdjNode(node2, node3);
		graph.addAdjNode(node3, node2);
		graph.addAdjNode(node3, node4);
		graph.addAdjNode(node4, node3);
		
		List<Node> a = new Dijkstra().search(graph, node1, node4);
		
		List<Node> b = new ArrayList<Node>();
		b.add(node4);
		b.add(node3);
		b.add(node2);
		b.add(node1);
		assertEquals(a, b);
	}
	
	@Test
	public void testPlusCourtCheminAStar() {
		Graph graph = new Graph();
		
		Node node1 = new Node(0, 0);
		Node node2 = new Node(1, 0);
		Node node3 = new Node(2, 0);
		Node node4 = new Node(3, 0);
		graph.addAdjNode(node1, node2);
		graph.addAdjNode(node2, node1);
		graph.addAdjNode(node2, node3);
		graph.addAdjNode(node3, node2);
		graph.addAdjNode(node3, node4);
		graph.addAdjNode(node4, node3);
		
		//List<Node> a = new AStar(new HeuristicNearest()).search(graph, node1, node4);
		List<Node> a = new AStar(new EuclideanDistance()).search(graph, node1, node4);
		
		List<Node> b = new ArrayList<Node>();
		b.add(node4);
		b.add(node3);
		b.add(node2);
		b.add(node1);
		assertEquals(a, b);
	}
	
}
