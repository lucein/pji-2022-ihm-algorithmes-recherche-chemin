package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Comparator;

import org.junit.Test;

import model.SortedList;

/**
 * Tests for the SortedList class
 * 
 * @author Lhomme Lucien
 *
 */
public class SortedListTests {

	private SortedList<Integer> createEmptySortedList() {
		return new SortedList<Integer>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				if (o1 < o2)
					return 1;
				else if (o1 == o2)
					return 0;
				else
					return -1;
			}
		});
	};
	
	private boolean isSorted(SortedList<Integer> sl) {
		Integer last = null;
		//Integer integer = null;
		System.out.println(sl);
		for (Integer integer : sl) {
		//while ((integer = sl.poll()) != null) {
			//System.out.println(last);
			//System.out.println(integer);
			//System.out.println();
			if (last != null) {
				if (last < integer) {
					return false;
				}
			}
			last = integer;
		}
		return true;
	}

	@Test
	public void testEmptySortedListPop() {
		SortedList<Integer> sl = createEmptySortedList();
		assertEquals(0, sl.size());
		assertThrows(IndexOutOfBoundsException.class , () -> sl.pop());
		assertEquals(0, sl.size());
	}

	@Test
	public void testEmptySortedListAdd() {
		SortedList<Integer> sl = createEmptySortedList();
		assertEquals(0, sl.size());
		sl.add(42);
		assertEquals(1, sl.size());

	}
	
	@Test
	public void testSortedListPop() {
		SortedList<Integer> sl = createEmptySortedList();
		sl.add(42);
		assertEquals(1, sl.size());
		assertEquals(42, sl.pop().intValue());
		assertEquals(0, sl.size());
	}
	
	@Test
	public void testSortedListAddNewValue() {
		SortedList<Integer> sl = createEmptySortedList();
		sl.add(1);
		sl.add(2);
		sl.add(3);
		sl.add(5);
		sl.add(6);
		assertEquals(5, sl.size());
		sl.add(4);
		assertEquals(6, sl.size());
		assertTrue(isSorted(sl));
	}
	
	@Test
	public void testSortedListAddSameValue() {
		SortedList<Integer> sl = createEmptySortedList();
		sl.add(1);
		sl.add(2);
		sl.add(3);
		sl.add(4);
		sl.add(5);
		assertEquals(5, sl.size());
		sl.add(0);
		assertEquals(6, sl.size());
		assertTrue(isSorted(sl));
	}
	
	@Test
	public void testSortedListContainsWithLowerCostThan() {
		SortedList<Integer> sl = createEmptySortedList();
		sl.add(1);
		sl.add(2);
		sl.add(3);
		sl.add(4);
		sl.add(5);
		/*assertTrue(sl.containsWithLowerCostThan(3, 5));
		assertFalse(sl.containsWithLowerCostThan(5, 3));
		assertFalse(sl.containsWithLowerCostThan(6, 5));
		assertFalse(sl.containsWithLowerCostThan(6, 1));
		assertFalse(sl.containsWithLowerCostThan(6, 6));
		*/
	}

}
