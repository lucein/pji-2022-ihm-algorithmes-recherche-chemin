package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Comparator;

import org.junit.Test;

import model.SortedList;

/**
 * Tests for the SortedList class
 * 
 * @author Lhomme Lucien
 *
 */
public class SortedListClassTests {

	static int totalNumber = 0;
	class MyClass {
		final int number;
		int value;
		public MyClass(int value) {
			this.number = ++totalNumber;
			this.value = value;
		}
	}

	private SortedList<MyClass> createEmptySortedList() {
		totalNumber = 0;
		return new SortedList<MyClass>(new Comparator<MyClass>() {
			@Override
			public int compare(MyClass o1, MyClass o2) {
				if (o1.value < o2.value)
					return -1;
				else if (o1.value == o2.value)
					return 0;
				else
					return 1;
			}
		});
	};
	
	private boolean isSorted(SortedList<MyClass> sl) {
		System.out.println("isSorted :");
		MyClass last = null;
		for (MyClass myClass : sl) {
			if (last != null) {
				if (last.value > myClass.value) {
					return false;
				}
			}
			System.out.println(myClass.number + " " + myClass.value);
			last = myClass;
		}
		return true;
	}
	
	@Test
	public void testEmptySortedListPop() {
		SortedList<MyClass> sl = createEmptySortedList();
		assertEquals(0, sl.size());
		assertThrows(IndexOutOfBoundsException.class, () -> sl.pop());
		assertEquals(0, sl.size());
	}

	@Test
	public void testEmptySortedListAdd() {
		SortedList<MyClass> sl = createEmptySortedList();
		assertEquals(0, sl.size());
		sl.add(new MyClass(42));
		assertEquals(1, sl.size());

	}
	
	@Test
	public void testSortedListPop() {
		SortedList<MyClass> sl = createEmptySortedList();
		sl.add(new MyClass(42));
		assertEquals(1, sl.size());
		assertEquals(42, sl.pop().value);
		assertEquals(0, sl.size());
	}
	
	@Test
	public void testSortedListAddNewValue() {
		SortedList<MyClass> sl = createEmptySortedList();
		sl.add(new MyClass(1));
		sl.add(new MyClass(2));
		sl.add(new MyClass(3));
		sl.add(new MyClass(5));
		sl.add(new MyClass(6));
		assertEquals(5, sl.size());
		sl.add(new MyClass(4));
		assertEquals(6, sl.size());
		assertTrue(isSorted(sl));
	}
	
	@Test
	public void testSortedListAddSameValue() {
		SortedList<MyClass> sl = createEmptySortedList();
		sl.add(new MyClass(1));
		sl.add(new MyClass(2));
		sl.add(new MyClass(3));
		sl.add(new MyClass(4));
		sl.add(new MyClass(5));
		assertEquals(5, sl.size());
		sl.add(new MyClass(1));
		sl.add(new MyClass(1));
		sl.add(new MyClass(1));
		assertTrue(isSorted(sl));
		assertEquals(1, sl.pop().number);
	}
	
	@Test
	public void testSortedListContainsWithLowerCostThan() {
		SortedList<MyClass> sl = createEmptySortedList();
		sl.add(new MyClass(1));
		sl.add(new MyClass(2));
		sl.add(new MyClass(3));
		sl.add(new MyClass(4));
		sl.add(new MyClass(5));
		//assertTrue(sl.containsWithLowerCostThan(3, 5));
		//assertFalse(sl.containsWithLowerCostThan(5, 3));
		//assertFalse(sl.containsWithLowerCostThan(6, 5));
		//assertFalse(sl.containsWithLowerCostThan(6, 1));
		//assertFalse(sl.containsWithLowerCostThan(6, 6));
	}

}
