package resources;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.scene.image.Image;

/**
 * Resources class which takes care of files.
 * 
 * @author Lhomme Lucien
 */
public abstract class Resources {
	
	/**
	 * Get the right bundle in the folder "resources/bundles".
	 * @param name String
	 * @return ResourceBundle
	 */
	public static ResourceBundle getBundle(String name) {
		return ResourceBundle.getBundle(String.format("resources/bundles.%s", name));
	}

	/**
	 * Get the right bundle in the folder "resources/bundles" for a specific language.
	 * @param name String
	 * @param locale Locale
	 * @return ResourceBundle
	 */
	public static ResourceBundle getBundle(String name, Locale locale) {
		return ResourceBundle.getBundle(String.format("resources/bundles.%s", name), locale);
	}
	
	/**
	 * Get the right image in the folder "resources/images".
	 * @param name String
	 * @return Image
	 */
	public static Image getImage(String name) {
		return new Image(Resources.class.getResourceAsStream(String.format("images/%s", name)));
	}
	
	/**
	 * Get the url of the path name.
	 * @param name String
	 * @return URL
	 */
	public static URL getURL(String name) {
		return Resources.class.getResource(name);
	}
	
}
