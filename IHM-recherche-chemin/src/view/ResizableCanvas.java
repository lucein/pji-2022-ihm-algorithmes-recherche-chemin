package view;

import java.util.function.BiConsumer;

import javafx.scene.canvas.Canvas;
import model.Graph;

public class ResizableCanvas extends Canvas {

	private BiConsumer<Canvas, Graph> draw;
	private Graph graph;

	public ResizableCanvas() {
		super(500, 500);
	}

	@Override
	public boolean isResizable() {
		return true;
	}

	@Override
	public double maxHeight(double width) {
		return Double.POSITIVE_INFINITY;
	}

	@Override
	public double maxWidth(double height) {
		return Double.POSITIVE_INFINITY;
	}

	@Override
	public double minWidth(double height) {
		return 1D;
	}

	@Override
	public double minHeight(double width) {
		return 1D;
	}

	@Override
	public void resize(double width, double height) {
		if (width < height) {
			this.setWidth(width);
			this.setHeight(width);
			draw();
		} else if (height < width) {
			this.setHeight(height);
			this.setWidth(height);
			draw();
		}
	}

	public void setDraw(BiConsumer<Canvas, Graph> draw, Graph graph) {
		this.draw = draw;
		this.graph = graph;
	}

	public void draw() {
		if (draw != null && graph != null)
			draw.accept(this, graph);
	}

}
