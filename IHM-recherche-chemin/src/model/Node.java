package model;

public class Node {

	private int x, y;

	private int cost, estimation, heuristic;

	public Node(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getEstimation() {
		return estimation;
	}

	public void setEstimation(int estimation) {
		this.estimation = estimation;
	}

	public int getHeuristic() {
		return heuristic;
	}

	public void setHeuristic(int heuristic) {
		this.heuristic = heuristic;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Node clone = new Node(x, y);
		return clone;
	}

	public Node copy() {
		try {
			return (Node) clone();
		} catch (CloneNotSupportedException e) {
			// NOT SUPPOSED TO BE THROWN
		}
		return null;
	}

}
