package model.labyrinth;

import model.Graph;
import model.Node;

public interface LabyrinthGenerator {

	public void generateLabyrinth(Graph graph, Node[][] nodes);
	
}
