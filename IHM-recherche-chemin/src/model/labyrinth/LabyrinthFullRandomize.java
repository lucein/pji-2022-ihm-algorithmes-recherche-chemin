package model.labyrinth;

import java.util.ArrayList;
import java.util.List;

import model.Graph;
import model.Node;

public class LabyrinthFullRandomize implements LabyrinthGenerator {

	@Override
	public void generateLabyrinth(Graph graph, Node[][] nodes) {
		for (int i = 0; i < 2*nodes.length*nodes.length; i++) {
			Node node = nodes[(int) (Math.random() * nodes.length)][(int) (Math.random() * nodes.length)];
			Node adj = chooseRandomAdj(graph, nodes, node);
			graph.addAdjNode(node, adj);
			graph.addAdjNode(adj, node);
		}
	}

	private Node chooseRandomAdj(Graph graph, Node[][] nodes, Node node) {
		List<Node> possibilities = new ArrayList<Node>();
		if (node.getX() > 0) {
			possibilities.add(nodes[node.getX()-1][node.getY()]);
		}
		if (node.getX() < nodes.length-1) {
			possibilities.add(nodes[node.getX()+1][node.getY()]);
		}
		if (node.getY() > 0) {
			possibilities.add(nodes[node.getX()][node.getY()-1]);
		}
		if (node.getY() < nodes.length-1) {
			possibilities.add(nodes[node.getX()][node.getY()+1]);
		}
		if (possibilities.isEmpty()) {
			return null;
		} else {
			return possibilities.get((int) (Math.random() * possibilities.size()));
		}
	}
}
