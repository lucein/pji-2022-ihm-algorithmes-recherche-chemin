package model.labyrinth;

import model.Graph;
import model.Node;

public class LabyrinthRecursiveDivision implements LabyrinthGenerator {

	@Override
	public void generateLabyrinth(Graph graph, Node[][] nodes) {
		repeteDivision(graph, nodes, 0, 0, nodes.length, nodes.length, chooseOrientation(nodes.length, nodes.length));
	}

	public void repeteDivision(Graph graph, Node[][] nodes, int x, int y, int width, int height, Orientation orientation) {
		if (width <= 1) {
			for (int i = 0; i < height - 1; i++) {
				graph.addAdjNode(nodes[x][y + i], nodes[x][y + i + 1]);
				graph.addAdjNode(nodes[x][y + i + 1], nodes[x][y + i]);
			}
			return;
		}
		if (height <= 1) {
			for (int i = 0; i < width - 1; i++) {
				graph.addAdjNode(nodes[x + i][y], nodes[x + i + 1][y]);
				graph.addAdjNode(nodes[x + i + 1][y], nodes[x + i][y]);
			}
			return;
		}

		// where will the wall be ?
		int wx = x + (orientation == Orientation.HORIZONTAL ? 0 : (int) (Math.random() * (width - 2)));
		int wy = y + (orientation == Orientation.HORIZONTAL ? (int) (Math.random() * (height - 2)) : 0);
		//int wx = x + (orientation == Orientation.HORIZONTAL ? 0 : width / 2-1);
		//int wy = y + (orientation == Orientation.HORIZONTAL ? height / 2-1: 0);

		// where will the passage through the wall exist ?
		int px = wx + (orientation == Orientation.HORIZONTAL ? (int) (Math.random() * width) : 0);
		int py = wy + (orientation == Orientation.HORIZONTAL ? 0 : (int) (Math.random() * height));
		//int px = wx + (orientation == Orientation.HORIZONTAL ? width/2: 0);
		//int py = wy + (orientation == Orientation.HORIZONTAL ? 0 :  height/2);

		// what direction will the wall be ?
		int dx = orientation == Orientation.HORIZONTAL ? 1 : 0;
		int dy = orientation == Orientation.HORIZONTAL ? 0 : 1;
		
		// how long will the wall be?
		int length = orientation == Orientation.HORIZONTAL ? width : height;

		wx += length * dx;
		wy += length * dy;
		
		int nx = x;
		int ny = y;
		int w = orientation == Orientation.HORIZONTAL ? width : wx - x + 1;
		int h = orientation == Orientation.HORIZONTAL ? wy - y + 1 : height;
		repeteDivision(graph, nodes, nx, ny, w, h, chooseOrientation(w, h));

		nx = orientation == Orientation.HORIZONTAL ? x : wx + 1;
		ny = orientation == Orientation.HORIZONTAL ? wy + 1 : y;
		w = orientation == Orientation.HORIZONTAL ? width : x + width - wx - 1;
		h = orientation == Orientation.HORIZONTAL ? y + height - wy - 1 : height;
		repeteDivision(graph, nodes, nx, ny, w, h, chooseOrientation(w, h));

		graph.addAdjNode(nodes[px][py], nodes[px + dy][py + dx]);
		graph.addAdjNode(nodes[px + dy][py + dx], nodes[px][py]);
	}

	public Orientation chooseOrientation(int width, int height) {
		if (width < height) {
			return Orientation.HORIZONTAL;
		} else if (height < width) {
			return Orientation.VERTICAL;
		} else {
			return Math.random() * 2 < 1 ? Orientation.HORIZONTAL : Orientation.VERTICAL;
		}
	}

}
