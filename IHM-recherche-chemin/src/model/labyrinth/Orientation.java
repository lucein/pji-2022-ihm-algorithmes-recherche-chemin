package model.labyrinth;

public enum Orientation {

	HORIZONTAL, VERTICAL;

}
