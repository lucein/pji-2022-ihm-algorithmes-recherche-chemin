package model.labyrinth;

import java.util.ArrayList;
import java.util.List;

import model.Graph;
import model.Node;

public class LabyrinthRecursiveBacktracking implements LabyrinthGenerator {

	@Override
	public void generateLabyrinth(Graph graph, Node[][] nodes) {
		repeteChoiceAdj(graph, nodes, nodes[(int) (Math.random() * nodes.length)][(int) (Math.random() * nodes.length)]);
	}

	public void repeteChoiceAdj(Graph graph, Node[][] nodes, Node node) {
		Node adj = null;
		while ((adj = chooseRandomAdj(graph, nodes, node)) != null) {
			graph.addAdjNode(node, adj);
			graph.addAdjNode(adj, node);
			repeteChoiceAdj(graph, nodes, adj);
		}
	}
	
	private Node chooseRandomAdj(Graph graph, Node[][] nodes, Node node) {
		List<Node> adjNodes = graph.getAdjNodes(node);
		List<Node> possibilities = new ArrayList<Node>();
		if (node.getX() > 0) {
			if (!adjNodes.contains(nodes[node.getX()-1][node.getY()])) {
				if (graph.getAdjNodes(nodes[node.getX()-1][node.getY()]).isEmpty()) {
					possibilities.add(nodes[node.getX()-1][node.getY()]);
				}
			}
		}
		if (node.getX() < nodes.length-1) {
			if (!adjNodes.contains(nodes[node.getX()+1][node.getY()])) {
				if (graph.getAdjNodes(nodes[node.getX()+1][node.getY()]).isEmpty()) {
					possibilities.add(nodes[node.getX()+1][node.getY()]);
				}
			}
		}
		if (node.getY() > 0) {
			if (!adjNodes.contains(nodes[node.getX()][node.getY()-1])) {
				if (graph.getAdjNodes(nodes[node.getX()][node.getY()-1]).isEmpty()) {
					possibilities.add(nodes[node.getX()][node.getY()-1]);
				}
			}
		}
		if (node.getY() < nodes.length-1) {
			if (!adjNodes.contains(nodes[node.getX()][node.getY()+1])) {
				if (graph.getAdjNodes(nodes[node.getX()][node.getY()+1]).isEmpty()) {
					possibilities.add(nodes[node.getX()][node.getY()+1]);
				}
			}
		}
		if (possibilities.isEmpty()) {
			return null;
		} else {
			return possibilities.get((int) (Math.random() * possibilities.size()));
		}
	}
	
}
