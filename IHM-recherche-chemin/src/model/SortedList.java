package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class SortedList<E> implements Iterable<E> {

	private List<E> list;

	private Comparator<E> comparator;

	public SortedList(Comparator<E> comparator) {
		list = new ArrayList<E>();
		this.comparator = comparator;
	}

	public void add(E e) {
		int insertionIndex = Collections.binarySearch(list, e, comparator);

		// < 0 if element is not in the list, see Collections.binarySearch
		if (insertionIndex < 0) {
			insertionIndex = -(insertionIndex + 1);
		} else {
			// Insertion index is index of existing element, to add new element
			// behind it increase index
			// while (insertionIndex < list.size() - 1 && comparator.compare(list.get(insertionIndex + 1), e) == 0) {
			// insertionIndex++;
			// }
			// insertionIndex++;
		}
		list.add(insertionIndex, e);
	}

	public E pop() {
		return list.remove(0);
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

	public int size() {
		return list.size();
	}

	@Override
	public Iterator<E> iterator() {
		return list.iterator();
	}

	public boolean contains(E e) {
		return Collections.binarySearch(list, e, comparator) >= 0;
	}
}
