package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Graph {

	private Map<Node, List<Node>> adjNodes;

	public Graph() {
		adjNodes = new HashMap<Node, List<Node>>();
	}

	public List<Node> getAdjNodes(Node node) {
		if (adjNodes.containsKey(node)) {
			return adjNodes.get(node);
		} else {
			List<Node> adjs = new ArrayList<Node>();
			adjNodes.put(node, adjs);
			return adjs;
		}
	}

	public void addAdjNode(Node node, Node adj) {
		List<Node> adjs = adjNodes.get(node);
		if (adjs == null)
			adjNodes.put(node, adjs = new ArrayList<Node>());
		if (!adjs.contains(adj))
			adjs.add(adj);
	}

	public void removeAdjNode(Node node) {
		for (Node adj : adjNodes.get(node))
			getAdjNodes(adj).remove(node);
		getAdjNodes(node).clear();
	}

	public Set<Entry<Node, List<Node>>> entrySet() {
		return adjNodes.entrySet();
	}

}
