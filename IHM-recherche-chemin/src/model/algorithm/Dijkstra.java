package model.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import model.Graph;
import model.Node;

public class Dijkstra extends Algorithm {

	private Map<Node, Integer> dist;
	private Map<Node, Node> predecesseur;

	public Dijkstra() {
		super("Dijkstra");
	}

	@Override
	public List<Node> search(Graph graph, Node sdeb, Node sfin) {
		dijkstra(graph, sdeb, sfin);
		List<Node> a = new ArrayList<Node>();
		Node s = sfin;
		StepAlgo step = new StepAlgo(StepEnum.GOOD);
		while (s != sdeb) {
			a.add(s);
			step.addNode(s);
			step.setOrigin(predecesseur.get(s));
			s = predecesseur.get(s);
			if (s == null) {
				setChanged();
				notifyObservers();
				return a;
			}
			// System.out.println(s.getX() + " " + s.getY());
		}
		a.add(sdeb);
		setChanged();
		notifyObservers(step);
		return a;
	}

	private void initialisation_dijkstra(Graph graph, Node sdeb) {
		dist = new HashMap<Node, Integer>();
		for (Entry<Node, List<Node>> entry : graph.entrySet()) {
			if (!entry.getValue().isEmpty()) {
				dist.put(entry.getKey(), Integer.MAX_VALUE);
			}
		}
		dist.put(sdeb, 0);
		predecesseur = new HashMap<Node, Node>();
	}

	private void dijkstra(Graph graph, Node sdeb, Node sfin) {
		initialisation_dijkstra(graph, sdeb);
		Set<Node> q = new HashSet<Node>(dist.keySet());
		while (!q.isEmpty()) {
			Node s1 = trouver_min(q);
			if (s1 == null) {
				return; // graph split
			}
			StepAlgo step = new StepAlgo(StepEnum.PASSED);
			step.addNode(s1);
			step.setOrigin(predecesseur.get(s1));
			setChanged();
			notifyObservers(step);
			q.remove(s1);
			StepAlgo step2 = new StepAlgo(StepEnum.CHECKED);
			for (Node s2 : graph.getAdjNodes(s1)) {
				if (q.contains(s2)) {
					maj_distances(s1, s2);
					if (s2 == sfin) {
						return;
					}
					step2.addNode(s2);
					step2.setOrigin(s1);
				}
			}
			setChanged();
			notifyObservers(step2);
		}
	}

	private Node trouver_min(Set<Node> q) {
		int mini = Integer.MAX_VALUE;
		Node sommet = null;
		for (Node s : q) {
			if (dist.get(s) < mini) {
				mini = dist.get(s);
				sommet = s;
			}
		}
		return sommet;
	}

	private void maj_distances(Node s1, Node s2) {
		if (dist.get(s2) > dist.get(s1) + 1) {
			dist.put(s2, dist.get(s1) + 1);
			predecesseur.put(s2, s1);
		}
	}

}
