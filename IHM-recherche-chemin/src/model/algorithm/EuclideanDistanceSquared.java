package model.algorithm;

import model.Node;

public class EuclideanDistanceSquared extends Heuristic {

	public EuclideanDistanceSquared(String name) {
		super(name);
	}

	public EuclideanDistanceSquared() {
		this("Euclidean distance, squared");
	}

	@Override
	public int heuristic(Node node, Node goal) {
		int dx = Math.abs(node.getX() - goal.getX());
		int dy = Math.abs(node.getY() - goal.getY());
		return dx * dx + dy * dy;
	}

}
