package model.algorithm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.Node;

public class StepAlgo implements Iterable<Node> {

	private StepEnum step;
	private Node origin;
	private List<Node> nodes;

	public StepAlgo(StepEnum step) {
		this.step = step;
		this.nodes = new ArrayList<Node>();
	}

	public StepEnum getStep() {
		return step;
	}

	public void addNode(Node node) {
		nodes.add(node);
	}

	public void addNodeFirst(Node node) {
		nodes.add(0, node);
	}

	@Override
	public Iterator<Node> iterator() {
		return nodes.iterator();
	}

	public int getSizeNodes() {
		return nodes.size();
	}

	public Node getOrigin() {
		return origin;
	}

	public void setOrigin(Node origin) {
		this.origin = origin;
	}
}
