package model.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Graph;
import model.Node;
import model.SortedList;

public class AStar extends Algorithm {

	private Heuristic heuristic;

	public AStar(final Heuristic heuristic) {
		super("A* " + heuristic.getName());
		this.heuristic = heuristic;
	}

	public List<Node> reconstituerChemin(Map<Node, Node> cameFrom, Node current) {
		List<Node> totalPath = new ArrayList<Node>(Arrays.asList(current));
		StepAlgo step = new StepAlgo(StepEnum.GOOD);
		step.addNode(current);
		step.setOrigin(cameFrom.get(current));
		while (cameFrom.containsKey(current)) {
			current = cameFrom.get(current);
			totalPath.add(current);
			if (cameFrom.containsKey(current)) {
				step.addNode(current);
			}
		}
		setChanged();
		notifyObservers(step);
		return totalPath;
	}

	@Override
	public List<Node> search(Graph graph, Node sdeb, Node sfin) {
		List<Node> closedList = new ArrayList<Node>();
		SortedList<Node> openList = new SortedList<Node>(new Comparator<Node>() {
			@Override
			public int compare(Node n1, Node n2) {
				if (n1.getHeuristic() < n2.getHeuristic())
					return -1;
				else if (n1.getHeuristic() == n2.getHeuristic())
					return n1.getEstimation() - n2.getEstimation();
				else
					return 1;
			}
		});
		updateNodeValues(sdeb, 0, heuristic.heuristic(sdeb, sfin));
		openList.add(sdeb);
		Map<Node, Node> cameFrom = new HashMap<Node, Node>();
		while (!openList.isEmpty()) {
			Node u = openList.pop();
			StepAlgo step = new StepAlgo(StepEnum.PASSED);
			step.addNode(u);
			step.setOrigin(cameFrom.get(u));
			setChanged();
			notifyObservers(step);
			if (u == sfin) {
				return reconstituerChemin(cameFrom, u);
			}
			StepAlgo step2 = new StepAlgo(StepEnum.CHECKED);
			for (Node v : graph.getAdjNodes(u)) {
				if (!openList.contains(v) && !closedList.contains(v)) {
					cameFrom.put(v, u);
					updateNodeValues(v, u.getCost() + 1, heuristic.heuristic(v, sfin));
					closedList.add(v);
					openList.add(v);
					step2.addNode(v);
					step2.setOrigin(u);
				} else {
					if (u.getCost() + 1 + heuristic.heuristic(v, sfin) < v.getHeuristic()) {
						cameFrom.put(v, u);
						updateNodeValues(v, u.getCost() + 1, heuristic.heuristic(v, sfin));
						if (closedList.contains(v)) {
							closedList.remove(v);
							openList.add(v);
						}
					}
				}
			}
			closedList.add(u);
			if (step2.getSizeNodes() > 0) {
				setChanged();
				notifyObservers(step2);
			}
		}
		return null; // terminer le programme (pas de chemin)
	}

	private void updateNodeValues(Node v, int cost, int estimation) {
		v.setCost(cost);
		v.setEstimation(estimation);
		v.setHeuristic(cost + estimation);
	}

}
