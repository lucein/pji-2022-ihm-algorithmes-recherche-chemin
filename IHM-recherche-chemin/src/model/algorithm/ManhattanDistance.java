package model.algorithm;

import model.Node;

public class ManhattanDistance extends Heuristic {

	public ManhattanDistance(String name) {
		super(name);
	}

	public ManhattanDistance() {
		this("Manhattan distance");
	}

	@Override
	public int heuristic(Node node, Node goal) {
		int dx = Math.abs(node.getX() - goal.getX());
		int dy = Math.abs(node.getY() - goal.getY());
		return dx + dy;
	}

}
