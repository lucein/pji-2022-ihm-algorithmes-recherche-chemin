package model.algorithm;

import java.util.List;
import java.util.Observable;

import model.Graph;
import model.Node;

/**
 * Abstract class which specify what is an algorithm.
 * 
 * @author Lhomme Lucien
 */
public abstract class Algorithm extends Observable {

	private String name;

	public Algorithm(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public abstract List<Node> search(Graph graph, Node sdeb, Node sfin);

}
