package model.algorithm;

import model.Node;

public class EuclideanDistance extends EuclideanDistanceSquared {

	public EuclideanDistance() {
		super("Euclidean distance");
	}

	@Override
	public int heuristic(Node node, Node goal) {
		return (int) Math.sqrt(super.heuristic(node, goal));
	}

}
