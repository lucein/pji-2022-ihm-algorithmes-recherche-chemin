package model.algorithm;

import model.Node;

public abstract class Heuristic {

	private String name;

	public Heuristic(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public abstract int heuristic(Node node, Node goal);

}
