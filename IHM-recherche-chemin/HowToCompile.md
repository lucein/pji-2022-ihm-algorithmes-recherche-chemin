# HowToCompile

java -version

## Java 8 (1.8) <- RECOMMENDED (JavaFX is a standard lib)

mkdir bin

javac -d bin src/view/*.java src/controller/*.java src/model/*.java src/model/labyrinth/*.java src/model/algorithm/*.java src/resources/*.java

cp src/resources/views/* bin/resources/views/
cp src/resources/bundles/* bin/resources/bundles/
cp src/resources/images/* bin/resources/images/

cd bin

jar -cvfe ../IHM-recherche-chemin.jar view.Main -C view controller .

cd ..

java -jar IHM-recherche-chemin.jar


## Java 11+ (1.11+) (JavaFX is a module lib) (https://gluonhq.com/products/javafx/)

mkdir bin

javac -d bin --module-path="D:\Applications\javafx-sdk-11.0.2\lib" --add-modules=javafx.base,javafx.controls,javafx.graphics,javafx.fxml src/view/*.java src/controller/*.java src/model/labyrinth/*.java src/model/algorithm/*.java src/model/*.java  src/resources/*.java

cp src/resources/views/* bin/resources/views/
cp src/resources/bundles/* bin/resources/bundles/
cp src/resources/images/* bin/resources/images/

cd bin

jar -cvfe ../IHM-recherche-chemin.jar view.Main -C view controller .

cd ..

java --module-path="D:\Applications\javafx-sdk-11.0.2\lib" --add-modules=javafx.base,javafx.controls,javafx.graphics,javafx.fxml -jar IHM-recherche-chemin.jar
