mkdir bin

chmod u+x java-11-openjdk-amd64/bin/javac
java-11-openjdk-amd64/bin/javac -d bin --module-path="D:\Applications\javafx-sdk-11.0.2\lib" --add-modules=javafx.base,javafx.controls,javafx.graphics,javafx.fxml src/view/*.java src/controller/*.java src/model/labyrinth/*.java src/model/algorithm/*.java src/model/*.java  src/resources/*.java

cp src/resources/views/* bin/resources/views/
cp src/resources/bundles/* bin/resources/bundles/
cp src/resources/images/* bin/resources/images/

cd bin

chmod u+x java-11-openjdk-amd64/bin/jar
java-11-openjdk-amd64/bin/jar -cvfe ../IHM-recherche-chemin.jar view.Main -C view controller .

cd ..
