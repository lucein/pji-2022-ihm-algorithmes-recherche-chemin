# PJI-2022-IHM-Algorithmes-recherche-chemin

PJI 2022 Visualisateur d'algorithmes de recherche de chemin

| Nom    | Prenom | Mail                            |
|--------|--------|---------------------------------|
| Lhomme | Lucien | lucien.lhomme.etu@univ-lille.fr |

## Introduction 

- IHM-recherche-chemin est un projet d'IHM Java se basant sur la librairie graphique JavaFX.

## Prérequis 

- La librairie graphique JavaFX est compris dans le JDK 8 de Java.

- Le répertoire "JDK-8" est un celui pour Windows.

- Le répertoire "java-11-openjdk-amd64" est un celui pour Linux et "javafx-sdk-11.0.1" est sa labrairie graphique, ce choix est fait car certaines distributions Unix semble alterer le JDK 8 avec la version 11 et plus JavaFX est simplement fournit comme un module.

## Heuristiques

- Dijkstra
- Distance Euclidienne
- Distance Euclidienne au carré
- Distance de Manhattan

## Ajouter une heuristique pour A*

- Pour ajouter une heuristique pour A*, il faut creer une classe héritant de la classe abstraite "Heuristic" dans le package "model.algorithm" et redéfinir la fonction "int heuristic(Node node, Node goal)" de cette dernière.

- Puis mettre une instance de cette classe dans le constructeur de AStar et l'ajouter dans la liste "algorithms" dans la fonction "void initialize(URL location, ResourceBundle resources)" à la suite des autres :

```
ObservableList<Algorithm> algorithms = FXCollections.observableArrayList(defaultAlgorithm = new Dijkstra(), new AStar(new EuclideanDistance()), new AStar(new EuclideanDistanceSquared()), new AStar(new ManhattanDistance())/* , placer vos algorithms içi */);
```

## Compilation 

- Il existe des fichiers d'execution afin de simplifier la compilation du projet.

- Le fichier "compile.bat" est le compilateur pour Windows et "compile-linux.sh" est celui pour Linux.

- Le fichier "HowToCompile.md" est la synthèse des commandes utiliser dans les deux fichiers précédents.

## Execution 

- Il existe des fichiers executables afin de simplifier l'utilisation du projet.

- Le fichier "run.exe" est pour l'execution pour Windows et "run-linux.sh" est celui pour Linux.

